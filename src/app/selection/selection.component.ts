import { Component, OnInit } from '@angular/core';
import { CameraServiceService } from '../services/camera-service.service';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss']
})
export class SelectionComponent implements OnInit {

  public rover_name : string ;
  public camera : any;

  constructor(private cameraService:CameraServiceService) { }

  ngOnInit() {

    this.rover_name = 'curiosity';
    this.camera = 'fhaz';

  }

  public setCameraSelection() {

    this.cameraService.camera_selected = this.camera;
    console.log("Camera property in service", this.cameraService.camera_selected);

  }
}
