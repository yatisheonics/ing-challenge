import { Component, OnInit } from '@angular/core';
import { CameraServiceService } from '../services/camera-service.service';

@Component({
  selector: 'app-image-collection',
  templateUrl: './image-collection.component.html',
  styleUrls: ['./image-collection.component.scss']
})
export class ImageCollectionComponent implements OnInit {

  public camera_from_service : string = "Test";
  constructor(public cameraService: CameraServiceService) { 

    
  }


  ngOnInit() {

    this.camera_from_service = this.cameraService.camera_selected;
  }

}
