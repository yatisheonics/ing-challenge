import { TestBed } from '@angular/core/testing';

import { RoverServiceService } from './rover-service.service';

describe('RoverServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoverServiceService = TestBed.get(RoverServiceService);
    expect(service).toBeTruthy();
  });
});
