import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class RoverServiceService {

  constructor(private http: HttpClient) { 

  }


  rovers_names : Array<String> = ["Curiosity","Opportunity","Spirit"];

  cameras_names_curiosity : Array<String> = ["FHAZ","RHAZ","MAST","CHEMCAM","MAHLI","MARDI","NAVCAM"];
  cameras_names_opportunity : Array<String> = ["FHAZ","RHAZ","NAVCAM","PANCAM","MINITES"];
  cameras_names_spirit : Array<String> = ["FHAZ","RHAZ","NAVCAM","PANCAM","MINITES"];



  public getRoversFronNasa() {

    //return this.http.get("https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?api_key=zaB55gj9AHLdIDSCoKWiId1aYOYPkA6p3hkwihEp&sol=1800") ;

  }

  public getImages(url:string) {

    //PhotoArray : Array<any>();
    //PhotoArray  =  this.http.get(url);

    return this.http.get(url) .pipe(
      retry(1),
      catchError(this.handleError));

  }
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
        // client-side error
        errorMessage = `Error: ${error.error.message}`;
    } else {
        // server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
}


}
