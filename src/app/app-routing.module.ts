import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ImageDisplayComponent } from './image-display/image-display.component';


const routes: Routes = [{ path: 'image-display', component: ImageDisplayComponent },

//{ path: 'hero/:id',component: HeroDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
