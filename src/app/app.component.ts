import { Component, OnInit } from '@angular/core';
import { RoverServiceService } from './services/rover-service.service';
import {  takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';




enum RoverSelection {
  Curiosity = 0, Opportunity = 1 , Spirit = 2
  }

  @Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit {
  title = 'ing-search';
  public rover_names ;
  public camera_names = this.roverService.cameras_names_curiosity;
  public roverEnum  = RoverSelection;
  public selectedRover = this.roverEnum.Curiosity ;
  public selectedCamera = 0;
  public sol_selection = "";
  public date_selection = "";
  public selected_rover_name : any = "";
  public selected_camera_name : any = "";
  photos = [];
 
  constructor(private roverService : RoverServiceService) {


  }

  changeCamera() {}

  onSearchClick() {

    this.selected_rover_name = this.roverService.rovers_names[this.selectedRover];
    this.selected_camera_name = this.camera_names[this.selectedCamera];
    let url = "";
    //console.log("The date entered is ",this.date_selection);
    //console.log("The sol entered is" ,this.sol_selection);


    if ( this.sol_selection == "" ) {
       url = `http://localhost:8080/searchNASA?rover=${this.selected_rover_name}&earth_date=${this.date_selection}&camera=${this.selected_camera_name}`;    
    } else if ( this.date_selection == "" ) {
       url = `http://localhost:8080/searchNASA?rover=${this.selected_rover_name}&sol=${this.sol_selection}&camera=${this.selected_camera_name}`;      
      }
      //console.log("The url is " , url);
      this.roverService.getImages(url).subscribe((images :any[])=>{
        // console.log(images);
        this.photos = images;
      });
  }

  changeRover() {

    if (this.selectedRover == this.roverEnum.Curiosity ) {
      this.camera_names = this.roverService.cameras_names_curiosity;
    } else if (this.selectedRover == this.roverEnum.Opportunity ) {
      this.camera_names = this.roverService.cameras_names_opportunity;
    } else if (this.selectedRover == this.roverEnum.Spirit ) {
      this.camera_names = this.roverService.cameras_names_spirit;
    }


    console.log("Changing selectedRover" +this.selectedRover);
  }

  ngOnInit() {

    this.rover_names = this.roverService.rovers_names;
    //this.camera_names = this.roverService.cameras_names;
 
   /* this.roverService.getRoversFronNasa().subscribe(rovers=>{

    console.log(rovers);

   });
 */


  }

}
